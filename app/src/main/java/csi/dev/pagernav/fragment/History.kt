package csi.dev.pagernav.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import csi.dev.pagernav.R
import kotlinx.android.synthetic.main.add_fragment.view.*

class History: Fragment() {
    private var textFragment = ""

    companion object {
        fun newInstance(text: String): History{
            val fragment = History()
            val bundle = Bundle()
            bundle.putString("AddFragment", text)
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        textFragment = arguments?.get("AddFragment").toString()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.tvFragment.text = textFragment
    }
}