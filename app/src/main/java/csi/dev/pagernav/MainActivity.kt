package csi.dev.pagernav

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import csi.dev.pagernav.adapter.AdapterFragment
import csi.dev.pagernav.fragment.Appointment
import csi.dev.pagernav.fragment.Booking
import csi.dev.pagernav.fragment.History
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        setupViewPager(pager)
        tabs.setupWithViewPager(pager)

    }

    private fun setupViewPager(pager: ViewPager?){
        val adapter = AdapterFragment(supportFragmentManager)

        val f1 = Booking.newInstance("Booking")
        adapter.Booking(f1, "Booking")

        val f2 = Appointment.newInstance("List Appointment")
        adapter.Appointment(f2, "List Appointment")

        val f3 = History.newInstance("History")
        adapter.History(f3, "History")

        pager?.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuProfile ->
                showToast("You clicked Profile")

            R.id.menuPassword ->
                showToast("You clicked Password")

            R.id.menuTerm ->
                showToast("You clicked Term")

            R.id.menuFaq ->
                showToast("You clicked FAQ")
        }
        return true
    }

    private fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }



}
