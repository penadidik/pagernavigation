package csi.dev.pagernav.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class AdapterFragment(manager: FragmentManager): FragmentPagerAdapter(manager) {

    private val fragments = ArrayList<Fragment>()
    private val titles = ArrayList<String>()

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size

    override fun getPageTitle(position: Int): CharSequence? = titles[position]

    fun Booking(fragment: Fragment, title: String){
        fragments.add(fragment)
        titles.add(title)
    }

    fun Appointment(fragment: Fragment, title: String){
        fragments.add(fragment)
        titles.add(title)
    }

    fun History(fragment: Fragment, title: String){
        fragments.add(fragment)
        titles.add(title)
    }
}